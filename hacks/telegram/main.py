import time

import telebot
from hacks.settings.conf import *
from hacks.telegram.buttons_creator import create_keyboard_by_texts
from hacks.utils.async_client import clients
from hacks.validates.service_ans import RequestApp

bot = telebot.TeleBot(TOKEN_TELEGRAM)


@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(
        message.chat.id,
        "Опишите вашу проблему с ребенком, мы поймем и дадим совет. \nХватит одного предложения."
    )


@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    request = RequestApp(
        platform="telegram",
        body=message.text,
        user_id=message.chat.id
    )
    response = clients.app.send_message_platform(request)
    if response is not None:
        # params = dict(
        #     chat_id=message.chat.id,
        #     text=response.text,
        # )
        # keyboard = response.get_keyboards_telegram(create_keyboard_by_texts)
        # if keyboard is not None:
        #     params["reply_markup"] = keyboard
        pass
    else:
        params = dict(
            chat_id=message.chat.id,
            text="Упс, Вы открыли новый уровень!",
        )
        bot.send_message(**params)


def start_bot():
    while True:
        try:
            bot.polling(none_stop=True, interval=0)
        except Exception as err:
            print(err)
            time.sleep(5)
