from flask import Flask, request, Response
from viberbot import Api
from viberbot.api.bot_configuration import BotConfiguration
from viberbot.api.messages import VideoMessage, KeyboardMessage
from viberbot.api.messages.text_message import TextMessage
import logging
from hacks.settings.conf import TOKEN_VIBER
from viberbot.api.viber_requests import ViberConversationStartedRequest
from viberbot.api.viber_requests import ViberFailedRequest
from viberbot.api.viber_requests import ViberMessageRequest
from viberbot.api.viber_requests import ViberSubscribedRequest
from viberbot.api.viber_requests import ViberUnsubscribedRequest

from hacks.utils.async_client import clients
from hacks.validates.service_ans import RequestApp

app = Flask(__name__)
viber = Api(BotConfiguration(
    name='Давай поговорим',
    avatar='https://psv4.userapi.com/c856228/u243615417/docs/d15/04a5e0dde1d3/avatarka.jpg?extra=-uS7aMsHbliC66YYNfAcBSucjcc7YzdlekndsaB0uWSfVF0iGc1qQ9a4k2BHXfG9QuErYGpbipzlSMerOOR_IA4Qwa-Y34JUQXVH7fZgDSliyxJ5GdN7MUfLv_14VbHMaZwaOheDXyBbSo2YNyyBInuZGw',
    auth_token=TOKEN_VIBER
))


@app.route('/', methods=['POST'])
def incoming():
    print("received request. post data: {0}".format(request.get_data()))
    # every viber message is signed, you can verify the signature using this method
    if not viber.verify_signature(request.get_data(), request.headers.get('X-Viber-Content-Signature')):
        return Response(status=403)

    # this library supplies a simple way to receive a request object
    viber_request = viber.parse_request(request.get_data())

    if isinstance(viber_request, ViberMessageRequest):
        message = viber_request.message
        # lets echo back
        request_app = RequestApp(
            platform="viber",
            body=message.text,
            user_id=viber_request.sender.id
        )
        print("message|", message.text)
        response_app = clients.app.send_message_platform(request_app)
        if response_app is None:
            viber.send_messages(viber_request.sender.id, [
                TextMessage(text="Упс, Вы открыли новый уровень!")
            ])
    elif isinstance(viber_request, ViberSubscribedRequest):
        viber.send_messages(viber_request.get_user.id, [
            TextMessage(text="Вы новый подписчик бота")
        ])
    elif isinstance(viber_request, ViberFailedRequest):
        print("client failed receiving message. failure: {0}".format(viber_request))

    return Response(status=200)


if __name__ == "__main__":
    # context = ('server.crt', 'server.key')
    # app.run(host='0.0.0.0', port=443, debug=True, ssl_context=context)
    app.run(host='0.0.0.0', port=80, debug=False)
