import os
import uuid

import trio
import async_vk_api
import async_vk_bot
from hacks.settings.conf import VK_API_ACCESS_TOKEN
from hacks.utils.async_client import clients
from hacks.validates.service_ans import RequestApp


def new_message(event):
    return event['type'] == 'message_new'


async def echo_once(bot):
    """
    Waits for a new message and sends the received text back exactly once.
    """
    event = await bot.wait(new_message)
    await bot.api.messages.send(
        peer_id=event['object']['peer_id'],
        message=event['object']['text']
    )


async def echo(bot):
    """
    Waits for new messages and sends the received text back.
    """
    async with bot.sub(new_message) as events:
        async for event in events:
            request = RequestApp(
                platform="vk",
                body=event['object']['body'],
                user_id=event['object']['user_id']
            )
            response = clients.app.send_message_platform(request)
            if response is not None:
                # await bot.api.messages.send(
                #     peer_id=event['object']['user_id'],
                #     message=response.text,
                #     keyboard=response.get_keyboards_vk(),
                #     random_id=uuid.uuid4().int & (1 << 32) - 1
                # ) IS OK
                pass
            else:
                await bot.api.messages.send(
                    peer_id=event['object']['user_id'],
                    message="Упс, Вы открыли новый уровень!",
                    random_id=uuid.uuid4().int & (1 << 32) - 1
                )


async def main():
    """
    Starts the bot and event handlers.
    """
    access_token = VK_API_ACCESS_TOKEN
    api = async_vk_api.make_api(access_token, version='5.126')
    bot = async_vk_bot.make_bot(api)

    async with trio.open_nursery() as nursery:
        nursery.start_soon(bot)
        nursery.start_soon(echo, bot)
