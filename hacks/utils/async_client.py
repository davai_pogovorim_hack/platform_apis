import uuid

import requests
from requests.exceptions import (
    ConnectionError,
    ConnectTimeout,
    ReadTimeout,
)

import aiohttp
import typing as t

from hacks.settings import conf as config
from hacks.validates.service_ans import RequestApp, ResponseAppStatus


class JsonRpcClientError(Exception):
    status: t.Optional[int]
    message: str
    data: t.Dict[str, t.Any]

    def __init__(self, status=None, message='', data=None):
        self.status = status
        self.message = message
        if data is None:
            data = dict()
        self.data = data
        super().__init__(f'{self.status}, message={self.message}')


class JsonRpcClient:
    """Базовый JsonRPC клиент."""
    #: адрес сервера
    _url: str
    _session: aiohttp.ClientSession
    _timeout: aiohttp.ClientTimeout

    def __init__(self, _url='', _timeout=None):
        self._url = _url
        self._timeout = _timeout

    @staticmethod
    def _get_id():
        return uuid.uuid4().int & (1 << 32) - 1

    def request(self, method, params, timeout=None, headers={}):
        data = {
            'id': self._get_id(),
            'jsonrpc': '2.0',
            'method': method,
            'params': params,
        }

        request_data = dict(
            url=self._url,
            json=data,
            headers=headers,
        )

        request_data['timeout'] = None

        with self._session.post(**request_data) as response:
            if response.status_code in [200, 201]:
                resp = response.json()
            if 'error' in resp:
                if isinstance(resp['error'], dict):
                    raise JsonRpcClientError(
                        status=resp['error'].get('code'),
                        message=resp['error'].get('message', ''),
                        data=resp['error'].get('data'),
                    )
                else:
                    raise JsonRpcClientError(
                        message=str(resp['error'])
                    )
            return resp['result']

    def request_safe(self, *args, **kwargs):
        try:
            return self.request(*args, **kwargs)
        except (
            JsonRpcClientError,
            ConnectionError,
            ConnectTimeout,
            ReadTimeout
        ) as e:
            print(e)
            return None

    def close(self):
        self._session.close()


class BaseClient(JsonRpcClient):
    """Основной класс клиента."""

    def __init__(self, verify_ssl=False):
        super().__init__(self._url)
        self._session = requests


class TownsApi(BaseClient):
    """Клиент для АПИ городов."""
    _url = config.APP_API

    def send_message_platform(
            self,
            params: RequestApp
    ) -> t.Optional[ResponseAppStatus]:
        res = self.request_safe('send_message_platform', params.dict())
        try:
            return ResponseAppStatus(**res)
        except Exception as err:
            print(err)
            return None


class Clients:
    _app: TownsApi

    def __init__(self):
        self._app = TownsApi()

    @property
    def app(self):
        return self._app


clients = Clients()
